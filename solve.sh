prj_root=$(pwd)

for d in pulsating-sphere-*/; do
    cd "${d}/elmerfem"
    ElmerSolver
    
    cd "${prj_root}"
    
done

