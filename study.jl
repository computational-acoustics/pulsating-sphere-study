#!/usr/bin/env julia

using PyCall
using JLD
using AcousticModels

meshio = pyimport("meshio")

function make_exact_field(
    points::AbstractArray, 
    U::Real,
    a::Real,
    f::Real,
    ρ::Real,
    c::Real
    )
    return spherical_wave.(
        U, 
        a, 
        f, 
        0, 
        sqrt.(points[:, 1].^2 + points[:, 2].^2 + points[:, 3].^2), 
        ρ, 
        c
        )
end


function extract_fem_field(mesh::PyObject)
    return mesh.point_data["pressure wave 1"] + im * mesh.point_data["pressure wave 2"]
end


function load_mesh(file_name::String)
    return meshio.read(file_name)
end


function write_mesh(file_name::String, mesh::PyObject)
    meshio.write(file_name, mesh)
end


function make_comparison_vtu(
    source_file_name::String, 
    destination_file_name::String,
    U::Real,
    a::Real,
    f::Real,
    ρ::Real,
    c::Real
    )
    
    mesh = load_mesh(source_file_name)
    
    fem_field = extract_fem_field(mesh)
    orig_size = size(fem_field)
    
    exact_field = make_exact_field(mesh.points, U, a, f, ρ, c)
    
    comparison_field = fem_field[:, 1] ./ exact_field
    
    field_dict = Dict(
        "pressure wave 1" => reshape(real.(comparison_field), orig_size),
        "pressure wave 2" => reshape(imag.(comparison_field), orig_size)
        )
    
    mesh.point_data = field_dict
    
    write_mesh(destination_file_name, mesh)
    
    return mesh.points, comparison_field
end


function make_comparison_files()

    # These inputs are the same as provided to the ElmerFEM model. Note that f is taken from the solver log, to be exactly the same as that used by ElmerFEM.
    f = 999.99999999999989
    U = 0.75
    c = 343.0
    ρ = 1.205
    a = 0.005

    # Edit if location of files differ.
    source_list = [
            "pulsating-sphere-1/elmerfem/case_t0001.vtu";
            "pulsating-sphere-2/elmerfem/case_t0001.vtu";
            "pulsating-sphere-3/elmerfem/case_t0001.vtu";
            "pulsating-sphere-4/elmerfem/case_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np1_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np2_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np3_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np4_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np5_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np6_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np7_t0001.vtu";
            "pulsating-sphere-5/elmerfem/case_8np8_t0001.vtu";
        ]
    
    # Edit as you wish.    
    dest_list = [
            "pulsating-sphere-1/elmerfem/check_t0001";
            "pulsating-sphere-2/elmerfem/check_t0001";
            "pulsating-sphere-3/elmerfem/check_t0001";
            "pulsating-sphere-4/elmerfem/check_t0001";
            "pulsating-sphere-5/elmerfem/check_8np1_t0001";
            "pulsating-sphere-5/elmerfem/check_8np2_t0001";
            "pulsating-sphere-5/elmerfem/check_8np3_t0001";
            "pulsating-sphere-5/elmerfem/check_8np4_t0001";
            "pulsating-sphere-5/elmerfem/check_8np5_t0001";
            "pulsating-sphere-5/elmerfem/check_8np6_t0001";
            "pulsating-sphere-5/elmerfem/check_8np7_t0001";
            "pulsating-sphere-5/elmerfem/check_8np8_t0001";
        ]
        
    for n in 1:length(source_list)
    
        points, c_field = make_comparison_vtu(
            source_list[n],
            dest_list[n] * ".vtu",
            U,
            a,
            f,
            ρ,
            c
            )
        
        save(dest_list[n] * ".jld", "points", points, "c_field", c_field)
        
    end
    
    # Convert a .pvtu file.
    pvtu = open("pulsating-sphere-5/elmerfem/case_t0001.pvtu") do file
        read(file, String)
    end
        
    open("pulsating-sphere-5/elmerfem/check_t0001.pvtu", "w") do f
        write(f, replace(pvtu, "case_" => "check_"))
    end
    
end

make_comparison_files()

