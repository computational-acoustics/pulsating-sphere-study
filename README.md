# Pulsating Sphere Study

Code to support comparison of the FEM solutions for the pulsating sphere with the analytical solution.

## Cloning This Repository

With recent versions of Git (higher than 2.13) use:

```bash
git clone --recurse-submodules -j$(nproc --all) https://gitlab.com/computational-acoustics/pulsating-sphere-study.git
cd pulsating-sphere-study/
```

Where the `-j` option is filled with the number of available cores. Alternatively, use:

```bash
git clone https://gitlab.com/computational-acoustics/pulsating-sphere-study.git
cd pulsating-sphere-study/
git submodule update --init --recursive
```

## Running this Study

Run the `solve.sh` script:

```bash
chmod +x solve.sh
./solve.sh
```

Then, run the `study.jl` Julia program.

```bash
julia study.jl
```

The `study.jl` program needs the following packages installed in your environment:

* [PyCall](https://github.com/JuliaPy/PyCall.jl), setup with a python environment in which [meshio](https://pypi.org/project/meshio/) is avaialable. For installation tips, see [here](https://gitlab.com/computational-acoustics/featools.jl#installation).
* [JLD](https://github.com/JuliaIO/JLD.jl).
* [AcousticModels](https://gitlab.com/computational-acoustics/acousticmodels.jl).

The `study.jl` program will output comparison fields with name `check_` in the various `pulsating-sphere-*/elmerfem` directories. These fields can be inspected with [ParaView](https://www.paraview.org/).

